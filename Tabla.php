<?php

    //Array que contiene nuestros datos, imaginate que es una base de datos o similar...
    $data = [
        [
            'nombre' => 'Coca Cola',
            'cantidad' => 100,
            'precio' => 4500,
        ],
        [
            'nombre' => 'Pepsi',
            'cantidad' => 30,
            'precio' => 4800,
        ],
        [
            'nombre' => 'Sprite',
            'cantidad' => 20,
            'precio' => 4500,
        ],
        [
            'nombre' => 'Guaraná',
            'cantidad' => 200,
            'precio' => 4500,
        ],
        [
            'nombre' => 'SevenUp',
            'cantidad' => 24,
            'precio' => 4800,
        ],
        [
            'nombre' => 'Mirinda Naranja',
            'cantidad' => 56,
            'precio' => 4800,
        ],
        [
            'nombre' => 'Mirinda Guaraná',
            'cantidad' => 89,
            'precio' => 4800,
        ],
        [
            'nombre' => 'Fanta Naranja',
            'cantidad' => 10,
            'precio' => 4500,
        ],
        [
            'nombre' => 'Fanta Piña',
            'cantidad' => 2,
            'precio' => 4500,
        ]
        
    ];
?>

<html>
	<head>
    <style>
    caption{
    	background-color: yellow;
        }
    th{
    	background-color: #cdcdcd;
        }
    tr:nth-child(odd){
    	background-color: #9FD1AC;
	}
    </style>
    </head>
    
    <body>
        <table border="1">
        	<caption> Productos </caption>
            <tr>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>Precio (Gs)</th>
            </tr>

            <?php

                $concat = '';

                foreach ($data as $productos) {

                    
                    $concat .= '<tr>';
                    $concat .= '<td>' . $productos['nombre'] .'</td>';
                    $concat .= '<td>' . $productos['cantidad'] .'</td>';
                    $concat .= '<td>' . number_format($productos['precio'], 0, '.','.') .'</td>';
                    $concat .= '</tr>';
                }

                echo $concat;
            ?>

        </table>
    </body>
</html>